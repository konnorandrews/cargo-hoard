use std::{
    collections::{BTreeMap, BTreeSet, HashMap, HashSet},
    mem,
    rc::Rc,
    task::Poll,
};

use log::*;

use cargo::{
    core::{
        dependency::DepKind,
        registry::PackageRegistry,
        resolver::{self, ResolveOpts, VersionPreferences},
        Dependency, FeatureValue, Package, PackageId, PackageSet, QueryKind,
        Registry, Source, SourceId, SourceMap, Summary, Target,
    },
    util::{interning::InternedString, VersionExt},
    Config,
};
use semver::Version;
use serde::{Deserialize, Serialize};
use simple_logger::SimpleLogger;
use time::OffsetDateTime;

fn get_page(page: u32) -> Vec<Crate> {
    info!("Getting page {}...", page);
    let mut resp: Crates = ureq::get(&format!(
        "https://crates.io/api/v1/crates?page={}&per_page=100&sort=downloads",
        page
    ))
    .set(
        "User-Agent",
        "cargo-hoard/1.0 (gitlab.com/konnorandrews/cargo-hoard)",
    )
    .call()
    .unwrap()
    .into_json()
    .unwrap();

    let now = time::OffsetDateTime::now_utc();

    resp.crates.retain(|c| {
        let likely_recently_used = c
            .recent_downloads
            .map(|downloads| downloads > 100)
            .unwrap_or(false);

        let active = likely_recently_used;
        if !active {
            warn!("{} is likely unmaintained", c.id);
        }
        active
    });

    // for c in &resp.crates {
    //     println!("{} - {}: {}", c.id, c.updated_at.date(), c.description.as_deref().unwrap_or(""))
    // }

    resp.crates
}

fn main() {
    SimpleLogger::new()
        .with_level(LevelFilter::Info)
        .with_module_level("cargo_hoard", LevelFilter::Debug)
        .init()
        .unwrap();

    // let crates: Vec<Crate> = (1..=100).flat_map(|i| get_page(i)).collect();
    // let exported_crates = serde_json::to_vec(&crates).unwrap();
    // std::fs::write("crates_io.top.json", exported_crates).unwrap();
    // dbg!(&resp);

    let crates: Vec<Crate> = serde_json::from_str(
        &std::fs::read_to_string("crates_io.top.json").unwrap(),
    )
    .unwrap();

    // let crates: Vec<Crate> = crates.into_iter().take(10000).collect();
    let crates = vec![Crate {
        description: None,
        id: "tokio".into(),
        downloads: 0,
        recent_downloads: None,
        updated_at: OffsetDateTime::now_utc(),
    }];

    // for c in resp.crates {
    //     let x = Dependency::parse(c.id, None, crates_io);
    //     dbg!(x);
    // }

    let config = Config::default().unwrap();
    let resolved_crates = find_all_dependancies_for_crates_io_crates(
        &config,
        &crates,
        // &[Crate {
        //     description: None,
        //     id: "rand".into(),
        //     downloads: 0,
        //     recent_downloads: None,
        // },
        // Crate {
        //     description: None,
        //     id: "tokio".into(),
        //     downloads: 0,
        //     recent_downloads: None,
        // }],
    );

    for (id, c) in resolved_crates {
        println!("{}", id)
    }
}

fn find_all_dependancies_for_crates_io_crates(
    config: &Config,
    crates: &[Crate],
) -> HashMap<PackageId, ResolvedCrate> {
    let crates_io_id = SourceId::crates_io(config).unwrap();
    let mut crates_io = crates_io_id.load(&config, &HashSet::new()).unwrap();
    let _lock = config.acquire_package_cache_lock().unwrap();

    // Find the newest non-prelease version in the registry.
    let package_ids = find_newest_package_ids(&mut crates_io, crates);
    info!("Top level packages: {}", package_ids.len());

    let package_ids = package_ids.into_iter().collect();

    let (resolved_crates, broken_crates, depth) =
        resolve_crate_pass(&config, &mut crates_io, &package_ids);

    info!("Total resolved: {}", resolved_crates.len());
    info!("Depth of resolve: {}", depth);
    info!(
        "Crates with unresolvable dependancies: {}",
        broken_crates.len()
    );

    for (id, (because, err)) in &broken_crates {
        if id == because {
            error!("Crate {} is unresolvable. Error:\n{}", id, err);
        } else {
            if package_ids.contains(id) {
                warn!("Crate {} is broken because of {}", id, because);
            }
        }
    }

    let good_crate_ids: HashSet<_> = resolved_crates
        .iter()
        .map(|(id, _)| *id)
        .filter(|id| !broken_crates.contains_key(id))
        .collect();
    let old_resolved_crates = resolved_crates;

    assert!(good_crate_ids.len() > 0);

    let (resolved_crates, broken_crates, depth) =
        resolve_crate_pass(&config, &mut crates_io, &good_crate_ids);

    info!("Total resolved: {}", resolved_crates.len());
    info!("Depth of resolve: {}", depth);
    info!(
        "Crates with unresolvable dependancies: {}",
        broken_crates.len()
    );

    for (id, _) in &resolved_crates {
        if old_resolved_crates.contains_key(id) {
        } else {
            error!("first stage never found crate {}", id);
        }
    }

    for (id, (because, err)) in &broken_crates {
        if good_crate_ids.contains(&id) {
            error!("first stage marked broken crate as good {}, it was broken by {}. Error:\n{}", id, because, err);
        } else {
        }
    }
    assert_eq!(broken_crates.len(), 0);

    resolved_crates
}

fn resolve_crate_pass(
    config: &Config,
    crates_io: &mut dyn Source,
    package_ids: &HashSet<PackageId>,
) -> (
    HashMap<PackageId, ResolvedCrate>,
    HashMap<PackageId, (PackageId, String)>,
    usize,
) {
    let mut dependants: HashMap<PackageId, HashSet<PackageId>> =
        package_ids.iter().map(|c| (*c, HashSet::new())).collect();

    let mut has_unresolvable_dependancies = HashMap::new();

    let mut resolved_crates = download_and_resolve_packages(
        config,
        crates_io,
        &package_ids,
        &mut dependants,
        &mut has_unresolvable_dependancies,
    );
    info!("Resolved top level crates: {}", resolved_crates.len());

    let mut crates_with_unresolved_dependancies: HashSet<PackageId> =
        resolved_crates.iter().map(|c| *c.0).collect();

    let mut registry = PackageRegistry::new(config).unwrap();
    registry.lock_patches();

    let mut i = 0;
    while crates_with_unresolved_dependancies.len() > 0 {
        i += 1;

        info!(
            "Resolving dependancies of {} from {} crates...",
            crates_with_unresolved_dependancies.len(),
            resolved_crates.len()
        );

        extend_resolved_crates(
            config,
            &mut registry,
            crates_io,
            &mut resolved_crates,
            &mut dependants,
            &mut has_unresolvable_dependancies,
            &mut crates_with_unresolved_dependancies,
        );
    }

    (resolved_crates, has_unresolvable_dependancies, i)
}

fn is_non_prerelease(summary: &&Summary) -> bool {
    !summary.version().is_prerelease()
}

fn by_version(summary: &&Summary) -> Version {
    summary.version().clone()
}

fn find_newest_package_ids(
    source: &mut dyn Source,
    crates: &[Crate],
) -> Vec<PackageId> {
    // Force cargo to load the index again.
    // crates_io.invalidate_cache();

    // Make sure we won't wait on the local index.
    source.block_until_ready().unwrap();

    let mut package_ids = Vec::new();

    for Crate { id, .. } in crates {
        // Ask cargo the known versions
        let dep = Dependency::parse(*id, None, source.source_id()).unwrap();
        let matches = match source.query_vec(&dep, QueryKind::Exact) {
            Poll::Ready(Ok(v)) => v,
            Poll::Ready(Err(e)) => panic!(),
            Poll::Pending => panic!(),
        };

        // Look for the newest non-prerelease version.
        // If we can't find a non-prerelease, then try to find the newest prerelease.
        if let Some(summary) = matches
            .iter()
            .filter(is_non_prerelease)
            .max_by_key(by_version)
        {
            debug!("Newest version for {}: {}", id, summary.package_id());
            package_ids.push(summary.package_id());
        } else if let Some(summary) = matches.iter().max_by_key(by_version) {
            warn!(
                "No non-prerelease versions found for {}, using prerelease {}",
                id,
                summary.package_id(),
            );
            package_ids.push(summary.package_id());
        } else {
            warn!("No versions found for {}, skipping", id);
        }
    }

    package_ids
}

fn download_and_resolve_packages(
    config: &Config,
    crates_io: &mut dyn Source,
    package_ids: &HashSet<PackageId>,
    dependants: &mut HashMap<PackageId, HashSet<PackageId>>,
    has_unresolvable_dependancies: &mut HashMap<PackageId, (PackageId, String)>,
) -> HashMap<PackageId, ResolvedCrate> {
    // Download the source for all the requested package IDs.
    let packages = bulk_download(config, crates_io, package_ids);

    // Find the library crates in each package and the features needed for all
    // dependencies to be resolved.
    let mut resolved_crates = HashMap::new();
    for download in packages {
        // If we find a binary only crate just ignore it.
        let lib_target = if let Some(lib_target) = download.library() {
            lib_target.clone()
        } else {
            warn!(
                "Package {} doesn't have a library crate, marking",
                download.package_id()
            );
            mark_as_unresolvable(
                dependants,
                has_unresolvable_dependancies,
                download.package_id(),
                None,
                "package doesn't have a library crate".into(),
            );
            continue;
        };

        let summary = download.summary().clone();
        assert_eq!(download.package_id(), summary.package_id());

        let dev_only_dependencies: HashSet<InternedString> = summary
            .dependencies()
            .iter()
            .filter(|dep| dep.kind() == DepKind::Development)
            .filter(|dep| {
                !summary.dependencies().iter().any(|other| {
                    other.kind() != DepKind::Development
                        && other.package_name() == dep.package_name()
                })
            })
            .map(|dep| dep.package_name())
            .collect();

        // Find the features needed to enable all the dependencies of the crate.
        let features = summary
            .features()
            .iter()
            .flat_map(|(feature, activates)| {
                let doesnt_activate_dev_only =
                    activates.iter().all(|value| match value {
                        FeatureValue::Feature(_) => true,
                        FeatureValue::Dep { dep_name } => {
                            !dev_only_dependencies.contains(dep_name)
                        }
                        FeatureValue::DepFeature { dep_name, .. } => {
                            !dev_only_dependencies.contains(dep_name)
                        }
                    });

                let enables_dep = activates.iter().any(|value| match value {
                    FeatureValue::Feature(_) => false,
                    FeatureValue::Dep { .. } => true,
                    FeatureValue::DepFeature { .. } => true,
                });

                if doesnt_activate_dev_only && enables_dep {
                    Some(*feature)
                } else {
                    None
                }
                // features.iter().flat_map(|feature| match feature {
                //     FeatureValue::Dep { dep_name } => {
                //         Some(format!("dep:{}", dep_name).into())
                //     }
                //     _ => None,
                // })
            })
            .collect();

        debug!(
            "Resolved crate for {}: {}, {:?}",
            summary.package_id(),
            lib_target.name(),
            features,
        );

        let dep = ResolvedCrate {
            summary,
            lib_target,
            features,
        };

        // We have resolved the library crate for the package.
        resolved_crates.insert(download.package_id(), dep);
    }

    resolved_crates
}

fn mark_as_unresolvable(
    dependants: &mut HashMap<PackageId, HashSet<PackageId>>,
    has_unresolvable_dependancies: &mut HashMap<PackageId, (PackageId, String)>,
    id: PackageId,
    because: Option<PackageId>,
    error: String,
) {
    let because = because.unwrap_or(id);

    let mut visited = HashSet::new();
    let mut to_mark_unresolvable = vec![id];
    while let Some(current_id) = to_mark_unresolvable.pop() {
        visited.insert(current_id);
        if has_unresolvable_dependancies
            .insert(current_id, (because, error.clone()))
            .is_none()
        {
            warn!(
                "Marking {} as having unresolvable dependancies because of {}",
                current_id, because
            );
        }

        // Queue dependants of this crate.
        for dep_id in dependants[&current_id].iter() {
            if visited.contains(dep_id) {
                // warn!("Dependency loop detected, {}", dep_id);
                panic!("Dependency loop detected with {}", id)
            } else {
                to_mark_unresolvable.push(*dep_id);
            }
        }
    }
}

fn extend_resolved_crates(
    config: &Config,
    registry: &mut PackageRegistry,
    source: &mut dyn Source,
    resolved_crates: &mut HashMap<PackageId, ResolvedCrate>,
    dependants: &mut HashMap<PackageId, HashSet<PackageId>>,
    has_unresolvable_dependancies: &mut HashMap<PackageId, (PackageId, String)>,
    crates_with_unresolved_dependancies: &mut HashSet<PackageId>,
) {
    let unresolved_summaries: Vec<_> = crates_with_unresolved_dependancies
        .iter()
        .map(|id| {
            let resolved = &resolved_crates[id];
            let features = &resolved.features;

            (
                resolved.summary.clone(),
                ResolveOpts {
                    dev_deps: false,
                    features:
                        resolver::features::RequestedFeatures::DepFeatures {
                            features: Rc::new(features.clone()),
                            uses_default_features: false,
                        },
                },
            )
        })
        .collect();

    let mut resolve_results = HashMap::new();
    let count = unresolved_summaries.len();
    for (i, summary) in unresolved_summaries.into_iter().enumerate() {
        let id = summary.0.package_id();
        debug!("Resolving dependancies of {}, {}/{}...", id, i + 1, count);

        match resolver::resolve(
            &[summary],
            &[],
            registry,
            &VersionPreferences::default(),
            None,
            false,
        ) {
            Ok(resolve) => {
                resolve_results.insert(id, resolve);
            }
            Err(err) => {
                todo!("check each feature individually to get around bad features (like cyclic dependancies) \
                if none of the features can be resolved (including no features) then mark it as actually unresolved.");
                warn!(
                    "Failed to resolve dependencies for {}, marking dependants \
                    as having an unresolvable dependency. Error:\n{}",
                    id, err
                );
                mark_as_unresolvable(
                    dependants,
                    has_unresolvable_dependancies,
                    id,
                    None,
                    err.to_string(),
                );
            }
        }
    }

    debug!("Done resolving dependancies");

    let mut new_packages = HashSet::new();

    // Find the new packages that we haven't resolved yet.
    let count = resolve_results.len();
    for (i, (id, resolve)) in resolve_results.into_iter().enumerate() {
        debug!("Looking up dependancies of {}, {}/{}", id, i + 1, count);

        for (dep_pkg, dep) in resolve.deps(id) {
            // We don't need dev dependancies.
            let needed = dep.iter().any(|dep| {
                dep.kind() == DepKind::Normal || dep.kind() == DepKind::Build
            });

            if needed {
                // Add this crate as a dependant of this dependancy.
                dependants
                    .entry(dep_pkg)
                    .or_insert(HashSet::new())
                    .insert(id);

                debug!("{} has dependancy on {}", id, dep_pkg);

                // Check if we have already resolved the package.
                // This either means we have already resolved the dependancies,
                // or we are currently doing that in this pass.
                if resolved_crates.contains_key(&dep_pkg) {
                    if let Some((because, err)) =
                        has_unresolvable_dependancies.get(&dep_pkg)
                    {
                        debug!(
                            "Package {} is unresolvable, marking {} as having \
                            unresolvable dependancies",
                            dep_pkg, id
                        );
                        mark_as_unresolvable(
                            dependants,
                            has_unresolvable_dependancies,
                            id,
                            Some(*because),
                            err.clone(),
                        );
                    } else {
                        debug!("Package {} has already been resolved", dep_pkg);
                    }
                } else {
                    // The package is new so we need to resolve it into a crate.
                    new_packages.insert(dep_pkg);
                }
            } else {
                debug!("{} is only a dev-dependency, skipping", dep_pkg);
            }
        }
    }

    // Mark the crates we just resolved dependancies for as done.
    crates_with_unresolved_dependancies.clear();

    if new_packages.is_empty() {
        return;
    }

    let new_resolved_crates = download_and_resolve_packages(
        config,
        source,
        &new_packages,
        dependants,
        has_unresolvable_dependancies,
    );

    for (k, c) in new_resolved_crates {
        if resolved_crates.contains_key(&k) {
            panic!("duplicate resolved crate")
        } else {
            resolved_crates.insert(k, c);
            crates_with_unresolved_dependancies.insert(k);
        }
    }
}

#[derive(Debug)]
struct ResolvedCrate {
    summary: Summary,
    lib_target: Target,
    features: BTreeSet<InternedString>,
}

fn bulk_download(
    config: &Config,
    source: &mut dyn Source,
    package_ids: &HashSet<PackageId>,
) -> Vec<Package> {
    source.block_until_ready().unwrap();

    let mut sources = SourceMap::new();
    sources.insert(Box::new(source));

    let package_ids: Vec<_> = package_ids.iter().copied().collect();

    let package_set = PackageSet::new(&package_ids, sources, config).unwrap();

    info!("Fetching {} packages...", package_ids.len());
    package_set
        .get_many(package_set.package_ids())
        .unwrap()
        .into_iter()
        .map(Package::clone)
        .collect()
}

#[derive(Deserialize, Serialize, Debug)]
struct Crates {
    crates: Vec<Crate>,
}

#[derive(Deserialize, Serialize, Debug)]
struct Crate {
    description: Option<String>,
    id: InternedString,
    downloads: u64,
    recent_downloads: Option<u64>,

    #[serde(with = "time::serde::iso8601")]
    updated_at: OffsetDateTime,
}
